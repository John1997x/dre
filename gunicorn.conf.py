'''
Gunicorn server configuration

Other options:
    https://docs.gunicorn.org/en/stable/settings.html#settings

To run simply do:

i. Use this file diretory as work directory
ii. Run Gunicorn:
    <path to your virtual env>/bin/gunicorn dre_django.wsgi
    For example:
    $HOME/.virtualenvs/dre/bin/gunicorn dre_django.wsgi
'''

from multiprocessing import cpu_count
import os
import sys
import os.path

project_dir = os.path.dirname(__file__)
lib_dir = os.path.join(project_dir, 'lib')
django_project = os.path.join(project_dir, 'dre_django')

sys.path.append(lib_dir)
sys.path.append(django_project)

bind = '127.0.0.1:' + os.environ.get('PORT', '8088')
max_requests = 1000
worker_class = 'gevent'
workers = cpu_count()
proc_name = 'dre'

# Debug
reload = True
