'''
Prepare the search query
'''

import re
import datetime

from haystack.query import SearchQuerySet
from dre_parser.simple_parser import SimpleParser

from .models import doc_type_str

abreviation_list = (
    # Use lower case abreviations and expansions
    # ( <abreviation>, <expansion> )
    (r'd\.?l\.?', 'decreto-lei'),
    (r'dec\.?\s*lei\.?', 'decreto-lei'),
    (r'd\.?\s*lei\.?', 'decreto-lei'),
    (r'dec\s*lei', 'decreto-lei'),
    (r'dec-lei', 'decreto-lei'),
    (r'decreto\s*lei', 'decreto-lei'),
)

alias_list = (
    (r'data:', 'date:'),
    (r'ano:', 'year:'),
    (r'dia:', 'day:'),
    (r'mês:', 'month:'),
    (r'tipo:', 'doc_type:'),
    (r'número:', 'number:'),
    (r'quem:', 'emiting_body:'),
    (r'fonte:', 'source:'),
    (r'notas:', 'notes:'),
    (r'série:', 'series:'),
    (r'dr:', 'dr_number:'),
    (r'começo:', 'start:'),
    (r'fim:', 'end:'),
    (r'parte:', 'part:'),
    (r'\.\.\.', r' '),
)

doc_ref_optimize = (r'(?ui)^(%s)(?:\s+número\s+|\s+n\.º\s+|\s+n\.\s+|\s+nº\s+|\s+n\s+|\s+)'
                    r'?([/\-a-zA-Z0-9]+)$' % doc_type_str)

doc_ref_optimize_re = re.compile(doc_ref_optimize)

other_massage_rules = (
    (doc_ref_optimize, r'doc_type:\1 number:\2'),
)

pre_massage_rules = abreviation_list + alias_list + other_massage_rules

tokenizer_rules = {
    'split_rules': r'(\"(?:[^\"\\]|\\.)*\"|[^\" ]+)',
    'merge_rules': [(lambda t, s: t[-1] == ':', lambda t, s: True, 'QuotedArg')],
}

# Lexer configuration

lex_simple_token = re.compile(r'(\"(?:[^\"\\]|\\.)*\"|[^\" ]+)')
lex_date_tag_token = re.compile(r'(?i)^(?P<tag>date):(?P<value>\d{4}-\d{1,2}-\d{1,2})$')
lex_start_tag_token = re.compile(r'(?i)^(?P<tag>start):(?P<value>\d{4}-\d{1,2}-\d{1,2})$')
lex_end_tag_token = re.compile(r'(?i)^(?P<tag>end):(?P<value>\d{4}-\d{1,2}-\d{1,2})$')
lex_tag_token = re.compile(r'(?i)^(doc_type|number|emiting_body|source|notes|series|'
                           r'dr_number|part):.*$')

lexicon = (
    ('DateTagToken', lambda t: lex_date_tag_token.match(t)),
    ('StartTagToken', lambda t: lex_start_tag_token.match(t)),
    ('EndTagToken', lambda t: lex_end_tag_token.match(t)),
    ('TagToken', lambda t: lex_tag_token.match(t)),
    ('SimpleToken', lambda t: lex_simple_token.match(t)),  # Must always be the last!
)

parser_rules = []
post_massage_rules = []
parser_transitions = []


def unquote(st):
    if st[0] == '"' and st[-1] == '"':
        return st[1:-1]
    return st


class QueryParser(SimpleParser):
    def tokens(self, txt):
        return (unquote(token.group(0))
                for token in re.finditer(self.split_rules, txt)
                if token)

    def tok_lexer(self, txt):
        self.state_setup()
        txt = self.massager(txt, self.pre_massage_rules)
        tokens = self.tokenizer(txt, sep='')
        tokens = list(self.lexer(tokens))
        return tokens


parse_query_optimized = QueryParser(
    abreviation_list + alias_list + other_massage_rules,  # pre_massage_rules
    tokenizer_rules,
    lexicon,
    parser_rules,
    post_massage_rules,
    parser_transitions)


parse_query_normal = QueryParser(
    abreviation_list + alias_list,  # pre_massage_rules
    tokenizer_rules,
    lexicon,
    parser_rules,
    post_massage_rules,
    parser_transitions)


def parse_query(query, optimized):
    '''
    Returns a SearchQuerySet
    '''
    # fonte:assembleia <<< falha
    search_query = SearchQuerySet()
    parser = parse_query_optimized if optimized else parse_query_normal

    tokens = parser.tok_lexer(query)
    query = []
    for t_type, token in tokens:
        if t_type == 'DateTagToken':
            try:
                date = datetime.datetime.strptime(token.split(':')[1], '%Y-%m-%d')
                search_query = search_query.filter(date__exact=date.date())
            except ValueError:
                query.append(token)
        elif t_type == 'StartTagToken':
            try:
                date = datetime.datetime.strptime(token.split(':')[1], '%Y-%m-%d')
                search_query = search_query.filter(date__gte=date.date())
            except ValueError:
                query.append(token)
        elif t_type == 'EndTagToken':
            try:
                date = datetime.datetime.strptime(token.split(':')[1], '%Y-%m-%d')
                search_query = search_query.filter(date__lte=date.date())
            except ValueError:
                query.append(token)
        elif t_type == 'TagToken':
            tag, value = token.split(':', 1)
            if tag == 'doc_type':
                search_query = search_query.filter(doc_type__exact=value)
            elif tag == 'number':
                search_query = search_query.filter(number__exact=value)
            elif tag == 'emiting_body':
                search_query = search_query.filter(emiting_body__exact=value)
            elif tag == 'source':
                search_query = search_query.filter(source__contains=value)
            elif tag == 'notes':
                search_query = search_query.filter(notes__contains=value)
            elif tag == 'series':
                search_query = search_query.filter(series__exact=value)
            elif tag == 'dr_number':
                search_query = search_query.filter(dr_number__contains=value)
            elif tag == 'part':
                search_query = search_query.filter(part__exact=value)
            else:
                query.append(token)
        else:
            query.append(token)

    if query:
        search_query = search_query.filter(content=' '.join(query))

    return search_query
