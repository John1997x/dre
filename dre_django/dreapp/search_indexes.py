import datetime
from haystack import indexes
from dreapp.models import Document


class DocumentIndex(indexes.SearchIndex, indexes.Indexable):
    date = indexes.DateField(model_attr='date')
    doc_type = indexes.CharField(model_attr='doc_type')
    number = indexes.CharField(model_attr='number')
    emiting_body = indexes.CharField(model_attr='emiting_body')
    source = indexes.CharField(model_attr='source')
    notes = indexes.CharField(model_attr='notes')
    series = indexes.IntegerField(model_attr='series')
    dr_number = indexes.CharField(model_attr='dr_number')
    part = indexes.CharField(model_attr='part')

    text = indexes.CharField(document=True, stored=False,
                             use_template=True,
                             template_name='document_text.txt')

    def get_model(self):
        return Document

    def index_queryset(self, using=None):
        '''Used when the entire index for model is updated.'''
        return self.get_model().objects.filter(timestamp__lte=datetime.datetime.now())

    def get_updated_field(self):
        return 'timestamp'
