'''
WSGI config for dre_django project.
'''

import os

from django.core.wsgi import get_wsgi_application

os.environ['DJANGO_SETTINGS_MODULE'] = 'dre_django.settings'

application = get_wsgi_application()
