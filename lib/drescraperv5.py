'''This module will scan and scrap the dre.pt site

This module had to be remade in order to read the new dre.pt site that went live on
2021-11-02.

It supersedes the drescraperv4 module.

Usage:

    # Create the reader for a given date 'date':
    day = DREReadDay(date)

    # Cycle through the docs present in 'date'.
    # These can be filtered using two sets of filters:

    # filter_dr - to filter out the series, in the form:
    # { 'no_series_[123]': <boolean value>, }

    # filter_doc - to filter document characteristics, any metadata field can
    # be used, for instance:
    # { 'doc_type': 'decreto', 'number':'58' }
    # If filter_doc is empty no filtering is made, if not a document must
    # match all the filters to be processed

    for doc in day.read_index(filter_dr, filter_doc):
        # Create the document object
        # The default options are:
        # default_options = {
        #        'update': False, # Do not update existing objects
        #        'update_metadata': True,
        #        'update_digesto': True, # Force digesto update
        #                                # even if we have it
        #        'update_inforce': True,
        #        'save_pdf': True,
        #        }
        update_obj = DREDocSave(doc, options)
        try:
            update_obj.save_doc()
        except DREDuplicateError:
            # These errors are ignored since we do not create duplicateds
            pass
'''

#
# Imports
#

# Global imports
import re
import sys
import os
import os.path
import datetime
import django
import requests
import logging
import json

# Django imports
from django.conf import settings
from django.core.exceptions import ObjectDoesNotExist
from django.db.models import Q
from django.db import IntegrityError

# App imports
from dreapp.models import (Document, DocumentText, NoIndexDocument)

# Local imports
from mix_utils import fetch_url
from json_payload import (DRS_BY_DATE, drs_by_date_url,
                          DOC_LIST_BY_DR, doc_list_by_dr_url,
                          GET_DOC, get_doc_url)

# Django environment
sys.path.append(os.path.abspath('../lib/'))
sys.path.append(os.path.abspath('../dre_django/'))
os.environ['DJANGO_SETTINGS_MODULE'] = 'dre_django.settings'
django.setup()

logger = logging.getLogger(__name__)

#
# Constants
#

COOKIES_URL = 'https://diariodarepublica.pt/dr/moduleservices/moduleversioninfo'
CSRF_URL = 'https://diariodarepublica.pt/dr/scripts/OutSystems.js'

CSRF_RE = re.compile(r'AnonymousCSRFToken="(.*?)"', re.MULTILINE)


SYNONYMS = [
    ['acórdão',
     'acórdão do supremo tribunal administrativo',
     'acórdão do supremo tribunal de justiça',
     'acórdão do tribunal constitucional',
     'acórdão do tribunal de contas', ],
    ['acordo colectivo de trabalho', 'acordo coletivo de trabalho'],
    ['alvará-extracto', 'alvará (extrato)'],
    ['anúncio-extracto', 'anúncio (extrato)'],
    ['aviso-extracto', 'aviso (extrato)'],
    ['contrato-extracto', 'contrato (extrato)'],
    ['declaração de rectificação', 'declaração de retificação'],
    ['declaração-extracto', 'declaração (extrato)'],
    ['decreto lei', 'decreto-lei'],
    ['deliberação-extracto', 'deliberação (extrato)'],
    ['despacho-extracto', 'despacho (extrato)'],
    ['directiva', 'diretiva'],
    ['listagem-extracto', 'listagem (extrato)'],
    ['portaria-extracto', 'portaria (extrato)'],
    ['regulamento-extracto', 'regulamento (extrato)'],
    ['relatório (extrato)', 'relatório-extrato'],
    ['decreto lei', 'decreto-lei'],
    ['decreto do presidente da república',
     'decreto',
     'decreto do representante da república para a região autónoma da '
     'madeira'],
    ['resolução',
     'resolução da assembleia da república',
     'resolução da assembleia legislativa da região autónoma da madeira',
     'resolução da assembleia legislativa da região autónoma dos açores',
     'resolução da assembleia nacional',
     'resolução do conselho de ministros', ],
]

NO_INDEX_CHECK = settings.NO_INDEX_CHECK
ARCHIVEDIR = settings.ARCHIVEDIR
MAX_ATTEMPTS = 5

#
# Utilities
#

# Exceptions


class DREParseError(Exception):
    '''
    Could not parse the document
    '''
    pass


class DREDuplicateError(Exception):
    '''
    Duplicated documents
    '''
    pass


class DREFileError(Exception):
    '''
    Error while downloading a file
    '''
    pass

# Functions


def strip_html(html):
    '''
    Inserts a line break between <p>aragraphs
    Removes repeated line breaks
    Removes html tags from a string
    '''
    html = html.replace('</p>', '\n').replace('\n\n', '\n')
    # Remove html tags
    txt = re.sub(r'<.*?>', '', html)
    return txt.strip()


def msg_doc(st, doc):
    data = doc.data
    return '%-15s ' % st + '%s %s of %s' % (data['doc_type'], data['number'],
                                            doc.journal.data['date'].date())


def check_dirs(date):
    archive_dir = os.path.join(ARCHIVEDIR,
                               '%d' % date.year,
                               '%02d' % date.month,
                               '%02d' % date.day)
    # Create the directory if needed
    if not os.path.exists(archive_dir):
        os.makedirs(archive_dir)
    return archive_dir


def save_file(filename, url):
    data_blob = fetch_url(url).content
    with open(filename, 'wb') as f:
        f.write(data_blob)
        f.close()

##
# Read the dre.pt site
##

# Re


# Journal number:
DR_NUMBER_RE = re.compile(r'^(?:Diário da República n.º |'
                          r'Diário do Governo n.º |'
                          r'Diário do Govêrno n.º )'
                          r'(?P<dr_number>[0-9A-Za-z/-]+)'
                          r'.*$')
# Journal dre.pt id
DR_ID_NUMBER_RE = re.compile(r'^https://dre.pt/web/guest/pesquisa-avancada/'
                             r'-/asearch/(?P<int_rd_num>\d+)/details/'
                             r'normal.*$')

CLAINT_RE = re.compile(r'https://dre.pt/web/guest/pesquisa-avancada/-'
                       r'/asearch/(?P<claint>\d+)/details/(?P<page>\d+)'
                       r'/normal.*$')

# Document type and number:

# On the 5th of March 2021 dre.pt ceased to have the name of the journal and its number on each of
# the document name and number headers. Because of this the following regexs were much simplified
# check commit before this day to see the original regexes
DOCTYPE_NUMBER_RE = re.compile(r'^(?P<doc_type>.*?)(?:\s+n.º\s+)'
                               r'(?P<number>[0-9A-Za-z\s/-]+).*$',
                               flags=re.U)

DOCTYPE_NONUMBER_RE = re.compile(r'^(?P<doc_type>.*?).*$')


# Connection manager

class ConnManager:
    '''
    The new dre.pt site uses POST methods to get the data to display on the
    site. The platform they use is protected against CSRF attacks by default,
    to do this they use a session cookie and a token that's used on the request
    header. This class manages this added complexity.
    '''
    def __init__(self):
        self.cookies = False
        self.CSRF_token = None
        self.session = requests.Session()
        self.age = 0

    def get_cookies(self):
        logger.info('Getting the session cookies')
        self.session.get(COOKIES_URL)
        self.cookies = True

    def get_CSRF_token(self):
        logger.info('Getting the CSRF token')
        result = self.session.get(CSRF_URL)
        match = CSRF_RE.search(result.text)
        self.CSRF_token = {'X-CSRFToken': match[1]}
        logger.debug(f'Got the token {self.CSRF_token}')

    def check_setup(self):
        self.age += 1
        if not self.cookies or not self.CSRF_token or self.age > 100:
            self.get_cookies()
            self.get_CSRF_token()
            self.age = 0

    def get_request(self, url, headers):
        self.check_setup()
        result = self.session.get(url, headers)
        result.raise_for_status()
        return result

    def post_request(self, url, payload):
        self.check_setup()
        # content-type: application/json; charset=utf-8
        result = self.session.post(url, json=payload, headers=self.CSRF_token)
        result.raise_for_status()
        return result


##
# Read the journals
##

class DREReadDoc(object):
    '''
    Document metadata reader
    '''

    def __init__(self, doc, journal):
        self.data = {}
        self.journal = journal
        self.parse(doc)

    ##
    # Parser

    def parse_emiting_body(self, doc):
        self.data['emiting_body'] = doc['Emissor']

    def parse_notes(self, doc):
        notes = strip_html(doc.get('Sumario', ''))
        self.data['notes'] = notes

    def parse_claint(self, doc):
        dip_legis = int(doc['DiplomaLegisId'])
        cont_pub = int(doc['ContratoPublicoId'])
        logger.debug('Choosing claint dip_legis=%d cont_pub=%d' % (dip_legis, cont_pub))
        self.data['claint'] = max((dip_legis, cont_pub))
        self.data['public_contrat'] = self.data['claint'] == int(doc['ContratoPublicoId'])

    def parse_pdfurl(self, tag):
        # Will only have the PDV when getting the document
        self.data['pdf_url'] = None

    def parse_doc_type_number(self, doc):
        title = doc.get('Titulo', '')
        # If we have document type and number:
        try:
            dtn = DOCTYPE_NUMBER_RE.match(title)
            self.data['doc_type'] = dtn.group('doc_type').strip()
            self.data['number'] = dtn.group('number').strip()
            return
        except AttributeError:
            pass

        # We have a document type but no number
        try:
            dtn = DOCTYPE_NONUMBER_RE.match(title)
            self.data['doc_type'] = dtn.group('doc_type').strip()
            self.data['number'] = ''
            return
        except AttributeError:
            pass

        # No document, no document number
        if 'Sem diploma' in title:
            self.data['doc_type'] = 'Não especificado'
            self.data['number'] = ''
            return

        raise DREParseError('Could not parse the document: %s' % title)

    def parse_digesto(self, tag):
        # TODO: remove the digesto entry
        self.data['digesto'] = self.data['claint']

    def parse_header(self, doc):
        '''
        TODO: review if the following is true on the new (2021) site. It should not be.

        Several types of headers are presented on the document listings:

        i) PDF link plus an rgba number:
            a) If the claint (the pdf number) and the rgba number are different
               then we have a digesto entry;
            b) If If the claint (the pdf number) and the rgba number the same
               there's no digesto entry: for example the first document, series
               1 from 2000-6-1 (Decreto do Presidente da República n.º
               27/2000);

        ii) No PDF link or claint
            Example: Second series, 1990-06-02
            These docs do not have claint, we make claint=-1 on these cases

        '''
        self.parse_claint(doc)
        self.parse_pdfurl(doc)
        self.parse_digesto(doc)
        self.parse_doc_type_number(doc)

        # For the documents that do not have text we used to define:
        # TODO: find out how to handle this on the new (2021) site.
        # self.parse_doc_type_number(header_text)
        # self.data['claint'] = -1
        # self.data['pdf_url'] = ''
        # self.data['digesto'] = None

    def parse(self, doc):
        self.parse_emiting_body(doc)
        self.parse_notes(doc)
        self.parse_header(doc)

    def filter(self, filter_doc={}):
        for fl in filter_doc:
            if not (filter_doc[fl].lower() in self.data[fl].lower()):
                return False
        return True


class DREReadJournal(object):
    '''
    Reads the journal index.
    '''

    def __init__(self, dr, date, conn):
        self.conn = conn
        self.data = {'date': date}
        self.parse(dr)

        # Abort if the date of the journal we're getting is different from the date we want
        # to get the journals from.
        assert self.data['dr_date'] == str(date.date()), 'The journal date is different from the current date'

        self.url = doc_list_by_dr_url
        self.payload = DOC_LIST_BY_DR
        self.payload['screenData']['variables']['DiarioId'] = int(self.data['dr_id_number'])

    def parse(self, dr):
        # Series
        title = dr['_source']['conteudoTitle'].lower()
        series = (1 if 'série i ' in title else
                  2 if 'série ii ' in title else
                  3 if 'série iii ' in title else None)

        # DR number
        dr_number = dr['_source']['numero']

        # Source
        source = dr['_source']['conteudoTitle']

        # Date
        dr_date = source.split()[-1]

        # DR internal id
        dr_id_number = dr['_source']['dbId']

        self.data.update({
            'series': series,
            'source': source,
            'dr_date': dr_date,
            'dr_number': dr_number,
            'dr_id_number': dr_id_number,
        })

    def filter(self, filter_dr={}):
        exclude_series = [filter_dr.get('no_series_1', False),
                          filter_dr.get('no_series_2', False),
                          filter_dr.get('no_series_3', True),
                          ]
        series = self.data['series']
        for i, fl in enumerate(exclude_series):
            if fl and series == i + 1:
                return False
        return True

    def get_result(self):
        result = self.conn.post_request(self.url, self.payload)
        result = result.json()['data']['DetalheConteudo']['List']
        return result

    def read_index(self):
        logger.debug('JOURNAL: Reading document list for %s, dr_id=%s' % (
            self.data['source'], self.data['dr_id_number']))
        result = self.get_result()
        for doc in result:
            try:
                yield DREReadDoc(doc, self)
            except DREParseError:
                pass


class DREReadDay(object):
    '''
    Reads the metadata of the 'date's list of journals

    On a given day we will have at least two journals, one from the first and
    another one from the second series.
    '''

    def __init__(self, date, conn):
        self.date = date
        self.url = drs_by_date_url
        self.payload = DRS_BY_DATE
        self.payload['clientVariables']['Data'] = str(self.date.date())
        self.conn = conn

    def get_result(self, filter_dr={}, filter_doc={}):
        result = self.conn.post_request(self.url, self.payload)
        result = json.loads(result.json()['data']['Json_Out'])['hits']['hits']
        return result

    def read_index(self, filter_dr={}, filter_doc={}):
        '''
        Create an index of documents on self.date
        '''
        # Get the list of journals for self.date
        logger.debug('JOURNAL: Reading journal list for %s' % self.date.date())
        result = self.get_result(filter_dr, filter_doc)
        dr_list = (DREReadJournal(journal, self.date, self.conn) for journal in result)

        # return the list of documents on each journal
        return (doc
                for dr in dr_list
                if dr.filter(filter_dr)
                for doc in dr.read_index()
                if doc.filter(filter_doc))


class Options(dict):
    '''Class to manage options.'''

    def __init__(self, options, default_options):
        dict.__init__(self, options)
        for opt in default_options:
            self[opt] = self.get(opt, default_options[opt])


NEW = 1
UPDATE = 2


class DREDocSave(object):

    def __init__(self, doc, options, conn):
        '''
        @param doc: DREReadDoc instance
        @param options: update options
        '''
        self.conn = conn
        self.payload = GET_DOC
        self.payload['screenData']['variables'] = {}
        if doc.data['public_contrat']:
            self.payload['screenData']['variables']['ContPubId'] = doc.data['claint']
            logger.debug('Payload ContPubId=%s' % [doc.data['claint']])
        else:
            self.payload['screenData']['variables']['DipLegisId'] = doc.data['claint']
            logger.debug('Payload DipLegisId=%s' % [doc.data['claint']])
        self.url = get_doc_url
        self.doc = doc
        self.mode = NEW
        default_options = {
            'update': False,
            'update_metadata': True,
            'update_digesto': True,
            'update_inforce': True,
            'update_notes': True,
            'update_part': True,
            'save_pdf': True,
        }
        self.options = Options(options, default_options)
        self.document_text = False
        self.digesto_soup = None  # Will cache the digesto soup used to get
        # the digesto html and the part number

    ##
    # Save metadata

    def check_olddocs(self):
        '''
        For dates before the site change we should try to verify
        the document duplication by other means (since the 'claint' changed
        on the new site)
        new_site_date = 2014-09-18
        new_new_site_date = 2021-11-02
        '''
        date = self.doc.journal.data['date']
        doc_type = self.doc.data['doc_type'].lower()
        number = self.doc.data['number']
        series = self.doc.journal.data['series']
        # Does the current doc_type have synonyms?
        doc_types = [doc_type]
        for sn in SYNONYMS:
            if doc_type in sn:
                doc_types = sn
        # Create a query for the synonyms:
        dt_qs = Q(doc_type__iexact=doc_types[0])
        for dt in doc_types[1:]:
            dt_qs = dt_qs | Q(doc_type__iexact=dt)
        # Query the database:
        dl = Document.objects.filter(
            date__exact=date).filter(
            dt_qs).filter(
            number__iexact=number).filter(
            series__exact=series)
        if len(dl) > 1:
            # We have a number of documents that, for a given date, have
            # duplicates with the same number and type. The dates can be
            # listed with:
            # select
            #   count(*), date, doc_type, number
            # from
            #   dreapp_document
            # where
            #   date < '2014-9-18'
            # group by
            #   date, doc_type, number
            # having
            #   count(*) > 1;
            raise DREDuplicateError('Multiple similar documents')
        elif len(dl) == 0:
            return None
        else:
            return dl[0]

    def check_duplicate(self):
        '''
        Returns the Document object if the document is duplicated.
        Otherwise a new object is returned
        '''
        check_list = [self.check_olddocs, ]
        for check in check_list:
            doc_obj = check()
            if doc_obj:
                self.mode = UPDATE
                return doc_obj
        return Document()

    def save_metadata(self, doc_obj):
        '''
        Saves the metadata to doc_obj
        '''
        doc_data = self.doc.data
        journal_data = self.doc.journal.data

        doc_obj.claint = doc_data['claint']
        doc_obj.doc_type = doc_data['doc_type']
        doc_obj.number = doc_data['number']
        doc_obj.emiting_body = doc_data['emiting_body']
        doc_obj.source = journal_data['source']
        doc_obj.dre_key = ''
        doc_obj.in_force = True
        doc_obj.conditional = False
        doc_obj.processing = False
        doc_obj.part = '1'
        doc_obj.date = journal_data['date']
        if self.mode == NEW or self.options['update_notes']:
            doc_obj.notes = doc_data['notes']
        doc_obj.dre_pdf = 'DUMMY'
        doc_obj.pdf_error = False
        doc_obj.dr_number = journal_data['dr_number']
        doc_obj.series = journal_data['series']
        doc_obj.timestamp = datetime.datetime.now()
        doc_obj.save()
        logger.debug('Metadata saved claint=%s id=%d' % (doc_obj.claint, doc_obj.id))

    def check_forgetme(self, doc_obj):
        '''
        Checks for a number of conditions upon which a document is added to
        the "no indexable" table (model NoIndexDocument)

        To update the NoIndexDocument table directly use:

        insert into dreapp_noindexdocument (document_id)
        select
            dreapp_document.id
        from
            dreapp_document
            left join dreapp_noindexdocument
            on dreapp_noindexdocument.document_id = dreapp_document.id
        where
            series=2 and
            (lower(doc_type) like '%contumácia%' or
             lower(emiting_body) like '%juízo cível do tribunal%' or
             lower(emiting_body) like '%juízo criminal%' or
             lower(emiting_body) like '%tribunal da comarca%' or
             lower(notes) like '%declaração de insolvência%' or
             lower(notes) like '%exoneração do passivo%' or
             lower(notes) like '%notificação dos credores%' or
             lower(notes) like '%autos de processo%') and
            dreapp_noindexdocument.document_id is null;
        '''
        if doc_obj.series != 2:
            # Consider only documents on the second series
            return

        obj = doc_obj.dict_repr()
        for key, check in NO_INDEX_CHECK:
            st = obj[key]
            if isinstance(st, list):
                st = ' '.join(st)
            st = st.lower()
            if check in st:
                try:
                    forgetme = NoIndexDocument()
                    forgetme.document = doc_obj
                    forgetme.save()
                except IntegrityError:
                    pass
                break

    ##
    # Update DIGESTO

    def check_digesto(self, doc_obj):
        '''Checks if the document already has the digesto text'''
        document_text = None
        try:
            document_text = DocumentText.objects.get(document=doc_obj)
        except ObjectDoesNotExist:
            pass
        return document_text

    def get_result(self):
        result = self.conn.post_request(self.url, self.payload)
        result = result.json()
        return result

    def get_digesto(self):
        '''Downloads the digesto text'''
        result = self.get_result()['data']['DetalheConteudo']
        self.doc.data['result'] = result
        text = result['Texto']

        # Update PDF and Text url
        self.doc.data['pdf_url'] = result['VersaoPDF']
        if not result['ELI']:
            key = '%s-%s' % (result['Numero'].replace('/', '-').lower(), result['Id'])
            title = 'https://dre.pt/dre/detalhe/doc/%s' % key
        else:
            title = result['ELI']
        self.doc.data['text_url'] = title
        return text

    def save_digesto(self, document_text, doc_obj, text):
        '''
        Saves the document text to the db
        '''
        document_text.document = doc_obj
        document_text.text_url = self.doc.data['text_url']
        document_text.text = text
        document_text.save()
        logger.debug('Digesto saved into document_text table with id=%d', document_text.id)
        self.document_text = True

        # Update document metadata with information only available on the digesto
        doc_obj.dre_pdf = self.doc.data['pdf_url']
        doc_obj.save()

    def process_digesto(self, doc_obj):
        '''
        Gets more information from the digesto system
        Extracts the document html text from the digesto system
        '''
        # Do we have a digesto entry? If not, return
        if not self.doc.data['digesto']:
            logger.debug(msg_doc('No digesto:', self.doc))
            return
        # Check for digesto text
        document_text = self.check_digesto(doc_obj)
        # If it does not exist or we have a forced update read the html
        if not document_text:
            logger.debug(msg_doc('New digesto:', self.doc))
            document_text = DocumentText()
        elif document_text and self.options['update_digesto']:
            logger.debug(msg_doc('Update digesto:', self.doc))
        else:
            logger.debug(msg_doc('Already have digesto:', self.doc))
            return
        # Get the digesto text
        text = self.get_digesto()
        if not text:
            logger.debug(msg_doc('No digesto text:', self.doc))
            return
        # Save the text
        self.save_digesto(document_text, doc_obj, text)

    ##
    # Update part

    def process_part(self, doc_obj):
        # We have to have the digesto result to be able to get this info. This should
        # fail for older documents.
        logger.debug(msg_doc('Part number for:', self.doc))
        result = self.doc.data['result']

        if 'Parte' in result:
            if result['Parte']:
                part = result['Parte'].split('-')[0].strip()
            else:
                part = 1
        doc_obj.part = part
        doc_obj.save()

    ##
    # Update inforce

    def update_inforce(self, doc_obj):
        # We have to have the digesto result to be able to get this info. This should
        # fail for older documents.
        result = self.doc.data['result']
        in_force = not(result['Vigencia']=='NAO_VIGENTE')
        doc_obj.in_force = in_force
        doc_obj.save()

    ##
    # Save PDF

    def save_pdf(self, doc_obj):
        # If we don't have a pdf to fetch we give up saving the pdf
        if not self.doc.data['pdf_url']:
            return
        # Save the PDF
        archive_dir = check_dirs(self.doc.journal.data['date'])
        pdf_file_name = os.path.join(archive_dir,
                                     'dre-%d.pdf' % doc_obj.id)
        try:
            save_file(pdf_file_name, self.doc.data['pdf_url'])
        except Exception as e:
            # There are some documents on dre.pt that point to a PDF but
            # return a 404 code. We are going to keep the info about the
            # PDF url in the metadata maybe in the future this is fixed
            # by dre.pt.
            if len(e.args) > 1 and e.args[1] == 404:
                return
            raise

    ##
    # Main Save

    def save_doc(self):
        # Check for document duplication
        doc_obj = self.check_duplicate()
        if self.mode == UPDATE:
            if not self.options['update']:
                logger.debug(msg_doc('IGNORING duplicated document:',
                                     self.doc))
                raise DREDuplicateError('Not going to process this doc.')
            else:
                logger.info(msg_doc('UPDATE mode:', self.doc))
                logger.debug('doc_obj: %s' % doc_obj)
        else:
            logger.warn(msg_doc('NEW mode:', self.doc))
        # Save metadata
        if self.mode == NEW or (self.mode == UPDATE and self.options['update_metadata']):
            logger.debug(msg_doc('Metadata:', self.doc))
            self.save_metadata(doc_obj)
            self.check_forgetme(doc_obj)
        # Save digesto
        if self.mode == NEW or (self.mode == UPDATE and self.options['update_digesto']):
            self.process_digesto(doc_obj)
        # Update part number
        if self.mode == NEW or (self.mode == UPDATE and self.options['update_part']):
            self.process_part(doc_obj)
        # Update inforce
        if self.mode == NEW or (self.mode == UPDATE and self.options['update_inforce']):
            logger.debug(msg_doc('Update inforce:', self.doc))
            self.update_inforce(doc_obj)
        # Save PDF
        if self.mode == NEW or (self.mode == UPDATE and self.options['save_pdf']):
            logger.debug(msg_doc('Get PDF:', self.doc))
            self.save_pdf(doc_obj)
