'''
These are the payloads needed to extract documents from dre.pt

Examples:

    Get the list of journals in a given date:

        print()
        print('#' * 80)
        DRS_BY_DATE['screenData']['variables']['DataCalendario'] = '2021-11-02'
        url = 'https://diariodarepublica.pt/dr/screenservices/dr/Home/home/DataActionGetDRByDataCalendario'
        result = conn.post_request(url, DRS_BY_DATE)
        pprint.pprint(json.loads(result.json()['data']['Json_Out'])['hits']['hits'])

    Get the list of documents in a given journal:

        print()
        print('#' * 80)
        # DOC_LIST_BY_DR['screenData']['variables']['DiarioId'] = 173690372
        DOC_LIST_BY_DR['screenData']['variables']['DiarioId'] = 85446
        url = 'https://diariodarepublica.pt/dr/screenservices/dr/Legislacao_Conteudos/ListaDiplomas/DataActionGetDados'
        result = conn.post_request(url, DOC_LIST_BY_DR)
        pprint.pprint(result.json()['data']['DetalheConteudo']['List'])

    Get a given document:

        print()
        print('#' * 80)
        url = 'https://diariodarepublica.pt/dr/screenservices/dr/Legislacao_Conteudos/Conteudo_Detalhe/DataActionGetConteudo2'
        # If we have a public contract, we must query 'ContPubId', 'DipLegisId' otherwise
        GET_DOC['screenData']['variables']['DipLegisId'] = '392382'
        # GET_DOC['screenData']['variables']['ContPubId'] = '75630001'
        result = conn.post_request(url, GET_DOC)
        pprint.pprint(result.json())
'''

# Get list of DRs issued on a given date
drs_by_date_url = ('https://diariodarepublica.pt/dr/screenservices/dr/Home'
                   '/home/DataActionGetDRByDataCalendario')
DRS_BY_DATE = {
    "versionInfo": {
        "moduleVersion": "t0iuwPI0g7fC3bq2sYBDLg",
        "apiVersion": "A00rktBtkSvxDLsFy+6mgg"
        },
    "viewName": "Home.home",
    "screenData": {
        "variables": {
            "ContagemLegConsolidada": "",
            "ContagemJurisprudencia": "",
            "UtilizadorGestorDeConteudo": False,
            "HasSerie1": True,
            "HasSerie2": True,
            "DataUltimaPublicacao": "2099-11-26",
            "IsRendered": True,
            "IsMobile": "",
            "ContagemLexionario": "",
            "SerieI": False,
            "_serieIInDataFetchStatus": 1,
            "DataCalendario": "2021-10-25",
            "_dataCalendarioInDataFetchStatus": 1
            }
        },
    "clientVariables": {
        "NewUser": "https://dre.pt/dre/utilizador/registar",
        "PesquisaAvancada": "https://dre.pt/dre/pesquisa-avancada",
        "NIC": "",
        "UtilizadorPortalIdOld": "0",
        "Login": "https://dre.pt/dre/utilizador/entrar",
        "TotalResultados": 0,
        "Search": False,
        "DicionarioJuridicoId": "0",
        "FullHTMLURL_EN": "https://dre.pt/dre/en",
        "Name": "",
        "ShowResult": False,
        "EntityId_Filter": 0,
        "BookId_Filter": 0,
        "Email": "",
        "StartIndex": 0,
        "paginaJson": "",
        "Pesquisa": "lei 5/96",
        "CookiePath": "/dre/",
        "DataInicial_Filter": "1900-01-01",
        "DiarioRepublicaId": "",
        "Query_Filter": "",
        "UtilizadorPortalId": "0",
        "t": "",
        "Session_GUID": "cd330488-e5cd-45af-abee-08c61ef3e3c0",
        "ActoLegislativoId_Filter": 0,
        "FullHTMLURL": "https://dre.pt/dre/home",
        "TipoDeUtilizador": "",
        "DataFinal_Filter": "1900-01-01",
        "GUID": "cf2169ce-5ccf-4615-a0ee-676206dab74f",
        "IsColecaoLegislacaoFilter": True
        }
    }

# Get the list of documents on a given DR
doc_list_by_dr_url = ('https://diariodarepublica.pt/dr/screenservices/dr/Legislacao_Conteudos/'
                      'ListaDiplomas/DataActionGetDadosAndApplicationSettings')

DOC_LIST_BY_DR = {
    "versionInfo": {
        "moduleVersion": "_pFUJ6BZnlSJcvlqEIwfzw",
        "apiVersion": "+a6sJlUq3037Hz200P3AQA"
    },
    "viewName": "Legislacao_Conteudos.Conteudo_Detalhe",
    "screenData": {
        "variables": {}
    },
    "clientVariables": {
        "ActoLegislativoId_Filter": 0,
        "BookId_Filter": 0,
        "CookiePath": "/dr/",
        "DataFinal_Filter": "1900-01-01",
        "DataInicial_Filter": "1900-01-01",
        "DateTime": "2023-12-29T14:47:29.053Z",
        "DiarioRepublicaId": "",
        "DicionarioJuridicoId": "0",
        "DiplomaConteudoId": "229915923",
        "EntityId_Filter": 0,
        "FullHTMLURL_EN": "https://diariodarepublica.pt/dr/en",
        "FullHTMLURL": "https://diariodarepublica.pt/dr/home",
        "GUID": "",
        "IsColecaoLegislacaoFilter": True,
        "Login": "https://diariodarepublica.pt/dr/utilizador/entrar",
        "NewUser": "https://diariodarepublica.pt/dr/utilizador/registar",
        "paginaJson": "",
        "Pesquisa": "",
        "PesquisaAvancada": "https://diariodarepublica.pt/dr/pesquisa-avancada",
        "Query_Filter": "",
        "Serie": False,
        "Session_GUID": "ebc6beb5-f8a8-4e60-ba62-63a7ff75c765",
        "ShowResult": False,
        "StartIndex": 0,
        "t": "",
        "TipoDeUtilizador": "",
        "UtilizadorPortalId": "0",
    }
}

# Get the document
get_doc_url = ("https://diariodarepublica.pt/dr/screenservices/dr/Legislacao_Conteudos"
               "/Conteudo_Detalhe/DataActionGetConteudoDataAndApplicationSettings")
GET_DOC = {
    "versionInfo": {
        "moduleVersion": "_pFUJ6BZnlSJcvlqEIwfzw",
        "apiVersion": "4VWZV37qLRe3iQx+55AlsA"
    },
    "viewName": "Legislacao_Conteudos.Conteudo_Detalhe",
    "screenData": {
        "variables": {}
    },
    "clientVariables": {
        "ActoLegislativoId_Filter": 0,
        "BookId_Filter": 0,
        "CookiePath": "/dr/",
        "Data": "2023-12-12",
        "DataFinal_Filter": "1900-01-01",
        "DataInicial_Filter": "1900-01-01",
        "DateTime": "2023-12-29T14:47:29.053Z",
        "DiarioRepublicaId": "",
        "DicionarioJuridicoId": "0",
        "DiplomaConteudoId": "225283575",
        "EntityId_Filter": 0,
        "FullHTMLURL_EN": "https://diariodarepublica.pt/dr/en",
        "FullHTMLURL": "https://diariodarepublica.pt/dr/home",
        "GUID": "",
        "IsColecaoLegislacaoFilter": True,
        "Login": "https://diariodarepublica.pt/dr/utilizador/entrar",
        "NewUser": "https://diariodarepublica.pt/dr/utilizador/registar",
        "paginaJson": "",
        "Pesquisa": "",
        "PesquisaAvancada": "https://diariodarepublica.pt/dr/pesquisa-avancada",
        "Query_Filter": "",
        "Serie": True,
        "Session_GUID": "ebc6beb5-f8a8-4e60-ba62-63a7ff75c765",
        "ShowResult": False,
        "StartIndex": 0,
        "t": "",
        "TipoDeUtilizador": "",
        "UtilizadorPortalId": "0",
    }
}
