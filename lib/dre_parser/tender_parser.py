'''
Tokenizer, lexer and parser - tender documents
'''

##
# Imports
##

import re
from .simple_parser import SimpleParser

##
# Parser configuration
##

# Text massager configuration

pre_massage_rules = (
    (r'Decreto - Lei', 'Decreto-Lei'),
    (r'DL nº', 'Decreto-Lei n.º'),
)

post_massage_rules = (
    (r' ;', ';'),
)

# Tokenizer configuration
t_is_header = re.compile(r'^(?:\d{1,2}|\d{1,2}\.\d{1,2})\s-\s.*$')
t_is_list = re.compile(r'^(?:[A-Z](?:\d+)?|\d+|[a-z0-9]+\.\d+)\s*-\s+.*$')


def is_header(t, s): return bool(t_is_header.match(t))
def is_upper(t, s): return t[:t.find(':')].isupper()
def any_str(t, s): return True
def is_semicolon(t, s): return t == ';'
def asks_for_continuation(t, s): return t and t[-1] in ','
def is_continuation(t, s): return t and t[0].islower() and not t_is_list.match(t)


tokenizer_rules = {
    'split_rules': r'(?:\n|(;))',
    'merge_rules': [
        (is_header, is_upper, 'Header'),
        (asks_for_continuation, any_str, 'Sentences'),
        (any_str, is_semicolon, 'Semicolon'),
        (any_str, is_continuation, 'The rest'),
    ],
}

# Lexer configuration
t_Ignore = re.compile(
    r'(?ui)^Diário\s+da\s+República,\s+2\.ª\s+série\s+-\s+N\.º\s+\d+\s+-'
    r'\s+\d+\s+de\s+[a-zç]+\s+de\s+\d{4}\s+-\s+'
    r'(?:Anúncio\s+de\s+procedimento|'
    r'Anúncio\s+de\s+concurso\s+urgente|'
    r'Declaração\s+de\s+retificação\s+de\s+anúncio|'
    r'Aviso\s+de\s+prorrogação\s+de\s+prazo)\s+'
    r'n\.º\s+\d+/\d{4}\s+-\s+Página\s+n\.º\s+\d+'
)
t_Header = re.compile(r'^\d{1,2}\s-\s.*$')
t_SubHeader = re.compile(r'^\d{1,2}\.\d{1,2}\s-\s.*$')
t_ListItem01 = re.compile(r'^.+:.+$')
t_ListItem02 = re.compile(r'^(?:[A-Z](?:\d+)?|\d|[a-z]\.\d)\s*-\s+.+$')
t_Paragraph = re.compile(r'^.*$')

lexicon = (
    (None, lambda t: not t),
    (None, lambda t: t_Ignore.match(t)),
    ('Header', lambda t: t_Header.match(t)),
    ('SubHeader', lambda t: t_SubHeader.match(t)),
    ('ListItem', lambda t: t_ListItem02.match(t)),
    ('ListItem', lambda t: t_ListItem01.match(t)),
    ('Paragraph', lambda t: t_Paragraph.match(t)),
)

# Parser configuration
parser_rules = {
    'Header': '<h1>%s</h1>',
    'SubHeader': '<h2>%s</h2>',
    'ListItem': '<p class="list_item">%s</p>',
    'Paragraph': '<p>%s</p>',
}

parse_tender = SimpleParser(
    pre_massage_rules,
    tokenizer_rules,
    lexicon,
    parser_rules,
    post_massage_rules)
