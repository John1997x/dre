'''Scraping application for the dre.pt site, a portuguese database of
legislation.
'''

# Imports

import socket
import time
import unicodedata
import random
import requests
from requests.exceptions import HTTPError

from drelog import logger
from dreerror import DREError

######################################################################


def debug_unicode(st):
    if isinstance(st, str):
        return unicodedata.normalize('NFKD', st).encode('ascii', 'ignore')
    else:
        return unicodedata.normalize('NFKD', str(st, 'ascii', 'ignore')).encode('ascii')


du = debug_unicode

######################################################################

# Socket timeout in seconds
socket.setdefaulttimeout(60)
MAXREPEAT = 3


def fetch_url(url, params={}):
    # Get the payload
    repeat = 1
    while repeat:
        try:
            logger.debug('Getting: %s' % url)
            response = requests.get(
                url,
                params=params,
                headers={
                    'Accept-Encoding': 'gzip; q=1.0, identity; q=0.5',
                    'User-agent': 'Mozilla/5.0 (compatible; MSIE 9.0; Windows NT 6.0; Trident/5.0;'
                                  ' chromeframe/11.0.696.57)'
                })
            if response:
                repeat = False
            else:
                logger.critical('HTTP Error, status code: %s' % response.status_code)
                raise DREError('Error condition on the site %s' % response.status_code,
                               response.status_code)
        except (socket.timeout, socket.error, HTTPError) as e:
            msg = str(e)
            repeat += 1
            if repeat > MAXREPEAT:
                logger.critical('Socket or http error! Aborting')
                raise
            logger.debug('Socket or Http error! Sleeping for 5 minutes - %s' % msg)
            time.sleep(300)
        except Exception as e:
            msg = str(e)
            status = e.args[1]
            repeat += 1
            if repeat > MAXREPEAT:
                logger.critical('HTTP Error! Aborting. Error repeated %d times: %s' %
                                (MAXREPEAT, msg))
                raise
            if status in (400, 404):
                logger.critical('HTTP Error %d - URL: %s' % (status, url))
                raise
            if status == 503:
                logger.critical('HTTP Error 503 - cache problem going to try again in 10 seconds.')
                time.sleep(10)
                continue

            logger.warn('HTTP Error! Sleeping for 5 minutes: %s' % msg)
            time.sleep(300)

    t = random.randint(1, 5)
    logger.debug('Sleeping %ds' % t)
    time.sleep(t)
    return response
