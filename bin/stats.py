#!/usr/bin/env python3

'''
Read Apache logs to the database.

Note: Apache can log directly to the databse, however this will add a certain
overhead to each request/response. Using a batch task such as this one we can
concentrate the load when the server is least loaded.
'''

##
# Imports

import django
import sys
import os.path

current_dir = os.path.abspath(os.path.dirname(__file__))

sys.path.append(os.path.abspath((os.path.join(current_dir, '../lib/'))))
sys.path.append(os.path.abspath(os.path.join(current_dir, '../dre_django/')))

os.environ['DJANGO_SETTINGS_MODULE'] = 'dre_django.settings'

django.setup()

from dre_stats import LogReader

##
# Read the logs

log_reader = LogReader()

log_reader.run()
